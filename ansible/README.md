# ANSIBLE TO BUILD A KUBERNETES CLUSTER
We will use an opensource project for this [kubespray](https://kubespray.io/#/)
Kubespray has been extracted on ansible/kubernetes/kubespray

## Methods to build a k8s cluster using Ansible kubespray project

Check the  [ link ](../docs/kubernetes/README.md) to check about the options of using ansibl building a k8s cluster.

# ANSIBLE TO DEPLOY BITAMINA D'S CI/CD TOOL (on the Kubernetes Cluster)

The detail about how to deploy Jenkins with its capabilities using ansible is depicted in the following [link](../docs/jenkins/README.md)
