# Packer to create a RHEL KVM image
Create Instance images from DVD isos with Packer for usage with Libvirt/KVM virtualization: RHEL-8.3, RHEL-8-4, Ubuntu-20.04, or AlmaLinux.

## Packer
Packer is an open source tool for creating identical machine images for multiple platforms from a single source configuration.
While building the images packer uses a builder that itself relies on an virtualization system that must be available in the same system. For showcase purpose, the build json scripts may rely on **QEMU** for Mac OS or Linux and **VirtualBox** and **Vagrant**

Packer does not replace configuration tools like ansible although it is able to use it as a provisioner. For RHEL images, as they rely on a subscription manager, we don't use provisioner within packer json as openstack can provide it more efficiently and because kickstart script are such a powerful capability we use at full extent in this example. Because the need of subscription Bitamina D automates up to the point of having a KVM or VDI image able to run on any commercial hiypervisor.
For the other linux distro we will use the provisioners type shell to prepare the image for the runtime virtual environment.

Finally Packer allows to use post-processors which, for our project, is a shell script that performs converting and uploading the raw image created into the Openstack project, or even, eventually, to upload it to the public cloud. The image will be uploaded to openstack as private, leaving to the user configuring it as shared or private as needed. The other post-processor section will export the image a vagrant box file.

## Requirements

The system where the build and upload will run must have:
- **Packer**

(MacOS)
```ShellSession
% brew install packer
....
OK: packer 1.7.8 installed

% packer -version
1.7.8
```
(Linux)
```ShellSession
% sudo yum install qemu #RH family
....
% packer -version
1.7.8
```
- **QEMU**

(MacOS)
```ShellSession
% brew install qemu
....
% ls /usr/local/bin/qemu-system-x86_64
/usr/local/bin/qemu-system-x86_64
```
(Linux)
```ShellSession
% sudo yum install qemu-kvm #RH family
....
% ls /usr/local/bin/qemu-system-x86_64
/usr/local/bin/qemu-system-x86_64
```

- **Vagrant**

(MacOS)
```ShellSession
% brew install vagrant
....
% vagrant -v
Vagrant 2.2.19
```
(Linux)
```ShellSession
% sudo yum install vagrant #RH family
....
% vagrant -v
Vagrant 2.2.19
```
- **VirtualBox**

(MacOS)
```ShellSession
% brew install virtualbox
```
(Linux)
```ShellSession
% sudo yum install virtualbox #RH family
```

## How to build your custom image

### Clone of the git repo and cd the packer folder:

(MacOS or Linux)
```ShellSession
% git clone -b <desired-branch> https://gitlab.com/pablo225/demo_automation.git
% cd demo_automation/packer
```
### Copy/Move the pre-downloaded ISOs file into ./isos directory (1.2GiB each)
(MacOS or Linux)
```ShellSession
% mv ~/Downloads/rhel-8.4-x86_64-dvd.iso ./isos/
% mv ~/Downloads/ubuntu-20.04.3-live-server-amd64.iso ./isos/
% cd demo_automation/packer
```
### Export the following variables (skip this step if you are not planning to upload your image to a private or public cloud)
if you run packer without exporting them the upload will not happen
(MacOS or Linux Openstack credentials)
```ShellSession
export OS_USERNAME='opestackID'
export OS_PASSWORD='openstackPW'
export OS_AUTH_URL=http://0.0.0.0:5000/v3
export OS_PROJECT_NAME='project name'
export OS_USER_DOMAIN_NAME=Default
export OS_PROJECT_DOMAIN_NAME=Default
export OS_IDENTITY_API_VERSION=3
```
(MacOS or Linux Azure credentials)
```ShellSession
To be implemented
```
(MacOS or Linux AWS credentials)
```ShellSession
To be implemented
```

### Run the build that matches your needs of OS and output format.
(MacOS without display to build KVM/VDI image using QEMU)
```ShellSession
% packer build build-on_MacOS-rhel-8.3_img.json
```
(MacOS with Cocoa display to build KVM/VDI image using QEMU, if you need to see the progress of the installation)
```ShellSession
% packer build build-on_MacOS_cocoa-rhel-8.3_img.json
% packer build build-on_MacOS_cocoa-ubuntu-20.04_img.json
%
```
(Linux without display to build KVM/VDI image using QEMU)
```ShellSession
% packer build build-on_Linux-rhel-8.3_img.json
% packer build build-on_MacOS-ubuntu-20.04_img.json
```
(Linux with display to build KVM/VDI image using QEMU)
```ShellSession
% sed 's#"headless": "true"#"headless": "false"#g' build-on_Linux-rhel-8.3_img.json > build-on_Linux_gkt-rhel-8.3_img.json
% packer build build-on_Linux_gkt-rhel-8.3.json
% packer build build-on_gkt-ubuntu-20.04_img.json
```
(Vagrant box file using VirtualBox)
```ShellSession
% sed 's#"headless": "true"#"headless": "false"#g' build-on_Linux-rhel-8.3_img.json > build-on_Linux_gkt-rhel-8.3_img.json
% packer build build-on_Linux_gkt-rhel-8.3.json
% packer build build-on_gkt-ubuntu-20.04_img.json
```

## Kickstart Files for RHEL 8
The reasons behind using QEMU with Packer to create the kvm image suitable for Bitamin D project are:
- The partitions given by the default kvm image that can be downloaded from Red Hat does not fit our the recommended partitioning RH recommends.
- The recommended automated installation method from RedHat is using kickstart files (see https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/performing_an_advanced_rhel_installation/performing_an_automated_installation_using_kickstart)
- The most suitable method to perform a RHEL-8 installation using kickstart files and DVD iso as the source is packer with qemu virtualization.

As mentioned above packer itself does not manage the specifics of the installation. In this project that is given by the kickstart file you will find in the ./http folder. What packer does is to provide an http server to make that file accessible during the installation.

If adjustments are needed on the installation steps those must be done in the files ./http/rhel-8.x-x86_64.cfg

## Subiquity fles for Ubuntu 20.04
Debian Preseed files are consider deprecated and will not work with ubuntu-20.04.3-live-server-amd64.iso. The new standard to build custom images in an automated way is using meta-data and user-data located for this project within http/subiquity. Customize it to suit specific needs as required. 
