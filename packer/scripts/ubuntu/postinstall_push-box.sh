#!/bin/bash
path_image="artifacts/virtualbox/"
name=$IMAGE_NAME
version=$IMAGE_VERSION
cd ${path_image}
vagrant box remove bitaminad-gi-ubuntu-20.04
vagrant box add --name bitaminad-gi-ubuntu-20.04 ubuntu-20.04-virtualbox.box
