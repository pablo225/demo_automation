#!/bin/bash

# Post-processor : doing something with the built image.
# Basically the goal is to place in the project the user
# has permissions as a private image.
# Use disable set to true to waive the push of the image to openstack.


# image, sufixes, img format, architecture ...
name=$IMAGE_NAME
version=$IMAGE_VERSION
image="${name}-${version}"
path_image="artifacts/qemu/${image}"
src_format=qcow2
des_format=raw
img_suffix=bd-x86_64

# go to the artifact folder
cd ${path_image}

# rename the image and convert to raw
ls -latr  packer-${image} ${image}-bd-x86_64.${src_format}
mv packer-${image} ${image}-bd-x86_64.${src_format}

# Convert the image to vdi format for vgrant and to raw for openstack.
qemu-img convert -f ${src_format} -O ${des_format} ${image}-bd-x86_64.${src_format} ${image}-bd-x86_64.img
VBoxManage convertfromraw ${image}-bd-x86_64.img --format vdi  ${image}-bd-x86_64.vdi


# Openstack credentials and server (Canniff). The push cannot happen with crendentials on Openstack.
if [ -z ${OS_USERNAME} ] || [ -z ${OS_PASSWORD} ] || [ -z $OS_AUTH_URL ] || [ -z ${OS_PROJECT_NAME} ] || \
   [ -z ${OS_USER_DOMAIN_NAME} ] || [ -z ${OS_PROJECT_DOMAIN_NAME} ] || [ -z ${OS_IDENTITY_API_VERSION} ] \
   || [ ${DISABLE_PUSH} == "true"  ]
then
  echo " Before lunching this script the following variables must be exported: \

         export OS_USERNAME='opestackID'
         export OS_PASSWORD='openstackPW'
         export OS_AUTH_URL=http://0.0.0.0:5000/v3
         export OS_PROJECT_NAME=iac_bitaminad_env_ntz
         export OS_USER_DOMAIN_NAME=Default
         export OS_PROJECT_DOMAIN_NAME=Default
         export OS_IDENTITY_API_VERSION=3
       "; exit 0;
else

# Push image to openstack
openstack image create --disk-format ${des_format}  --container-format bare --min-disk 11 \
--min-ram 512 --private --property architecture='x86_64' --file ${image}-bd-x86_64.img  ${image}-bd-x86_64

fi
