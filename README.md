# DEMO OF AUTOMATION TO BUILD ROLL OUT BITAMINA D
Bitamina D is a proof of concept of a various set of technologies that aim collectively to deliver end to end solutions with minimal human intervention.

[diagram here]

## Bitamina D's Golden Image

Bitamina D pursues objectives of safety and and full control of the core software the OS runs.
A golden image is an archetypal version of a cloned disk used as a template for various kinds of virtual network
hardware. It's a  master image because multiple copies of the disk as an OS installation and setup can be
created.

Bitamina D currently enables the build of golden images for Vagrant/VirtualBox, openstack and vmware. The same image can be pushed to the public clouds but following  certain requirements.

The following Linux distributions are the recommended ones for Bitamina D
Virtualization | Openstack | VirtualBox/Vagrant        | Public Cloud (AWS Azure)
-------------- | --------- | -----------------------   | -------------------------------
Linux flavour  | RHEL 8    | Ubuntu 20.04  AlmaLinux   | RHEL 8, AlmaLinux, Ubuntu 20.04

NOTE: AlmaLinux golden image automated build to be implemented soon.
### How to create your custom Bitamina D Golden image:
As the first step to implement Bitamin D architecture we strongly recommend to customize your Linux image despite the virtual of physical environment it will run, no matter if it is private cloud or public cloud, or even when the plan is to run it in a local environments.

To create VM's images from DVD isos we will use Packer in conjunction to Libvirt/KVM virtualization.

The result golden image can be uploaded to Openstack, run on Vagrant VirtualBox or be set as custom Linux image in your public cloud for building your own VM instances.
Detail instruction here: [link](docs/packer/README.md)

### How to create the infrastructure Bitamina D runs on:
Once we have created our own flavour of Linux OS the next step will be using it by creating the underlying infrastructure.
Bitamina D can run on standard VM on VMware openstack or public cloud or can run on a Kubernetes/Openshift cluster.
For showcase of technologies purpose we have selected an architecture based on Vanilla Kubernetes where Bitamina D will run.
Infrastructure as Code is the guiding principle to implement the solution on K8s, which in more detail consists of:

* A _Docker Registry_ to customize and control the source of the docker images. [link](docs/registry/README.md):

* A _Kubernetes Cluster_ will be the environment to run the CI tool (Jenkins) used to build, test and deploy Bitamina D application. However the CI tool can push the application to pretty much any cloud provider or on-premises infrastructure.
To build a fully managed Vanilla K8s cluster the recommended method is Ansible. See [link](docs/kubernetes/README.md)

* A _Jenkins Server_ to provide Continuous Integration / Continuous Delivery. The Build, test, and deployment of Bitamina D will be
in the hands of Jenkins. Jenkins can come up with the pipelines fully implemented. Follow the steps in this [link](docs/jenkins/README.md)

### How to verify if Bitamina D up and running?
As mentioned above Jenkins will be provisioned by Ansible with all needed to stand up Bitamina D application and get it running.
Run the pipeline demo_automation on the jenkins instance already built.
Next Bitamina D instance should be available on http://<kubernetes_nodes_ips>:30020/
See more details about deployment and validation on [link](docs/application/README.md)
