## Jenkins as the CI pipeline Engine
Jenkins is one of the most used CI tools. Jenkins integrates almost every type of automation solution. Hence, the choice we made to adopt it for the solution.

### Jenkins image
Setting up Jenkins for a production environment is out of scope here. The amount of controllers, nodes, and clouds depends on capacity and organizational preferences.

Jenkins for Bitamina D executes the build and deployment in a Vanilla Kubernetes infrastruture or over Openstack/VMWare VM's, or on VMs/K8s running on the cloud.

Of all possible options we decided to customize Jenkins Docker image for Bitamina D with the purpose of running Docker and Kubernetes client within a single Jenkins instance.

***How is the custom Jenkins image being built?***

1) Under ansible/jenkins/ there is a Dockerfile. To build the image execute:
```ShellSession
docker build -t bitaminad-jenkins:stable -f Dockerfile . && \
docker tag bitaminad-jenkins:stable 192.168.56.12:5000/bitaminad-jenkins:stable  
docker login 192.168.56.12:5000/
docker push 192.168.56.12:5000/bitaminad-jenkins:stable
```
Notice that the registry is insecure just for this basic case. So in the VM the image to build the daemon.json file must have:
```ShellSession
vagrant@k8s2:~$ cat /etc/docker/daemon.json
{
  "insecure-registries" : ["192.168.56.12:5000"]
}
```
This will be added in the future to the Ansible collections as a role and to the Vagrant provisioning scripts.

### Jenkins Up and Running

***How to bring up our Jenkins instance (controller and executioner)?***

1) A basic way would be just using 'docker run' on the VM's created for the registry using vagrant/Vagrantfile:
```ShellSession
docker run -p 8080:8080 -v /var/run/docker.sock:/var/run/docker.sock  --name jenkins 192.168.56.12:5000/bitaminad-jenkins:stable
```
When you access to http://192.168.56.12:8080/ Jenkins will ask you to get from the container a password --> plugins to install --> admin credentials and you will be _almost_ ready to setup your pipelines.
As we are building Bitamina D app using Docker and Kubernetes it is necessary to install Docker and Docker pipelines plugin in Jenkins.

2) Using Kubernetes and Ansible
* Locate the credentials to reach out to the kubernetes cluster (the admin.conf file found in kubespray/inventory/<inventoryname>/artifacts) and place it in ~/.ssh/.kube/, or better suitable place.
Be sure that in the file ansible/jenkins/group_var/all/all.yaml, the ```kubernetes_conf``` variable matches that path.
So, to validate the availability of the k8s built run:
```ShellSession
kubectl --kubeconfig <path to the kubeconfig file> version
```

* To deploy Jenkins as a CI/CD tool using Ansible we will launch a playbook/role located within ansible/jenkins.
This playbook relies on the python3-modules kubernetes and openshift (```pip install kubernetes openshift```). Also will require to have kubectl in the $PATH variable of the system where the playbook is going to run [how to install kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl-macos/).
Install ansible modules required:
```ShellSession
cd ansible/jenkins && \
sudo pip install -r requirements.txt
```
```ShellSession
cd ansible/jenkins && \
ansible-playbook --connection=local 369_jenkins_lts.yml
```
This playbook will return to ansible/jenkins/credentials/jenkins_admin_password.txt the admin password. Use it to complete the setup of plugins and admin account.

* Generate a token for the admin to use jenkins-cli remotely and add it to ansible/jenkins/group_vars/all/all.yml
```
jenkins_admintoken: "the token goes here"
```

* Right now the playbook does not install the necessary jenkins plugins: 'docker' and 'docker pipelines'. That must be installed manually until the feature is added to the ansible role 'jenkins'.

* Run the same playbook again to install the multipipeline jobs.
```ShellSession
cd ansible/jenkins && \
ansible-playbook --connection=local 369_jenkins_lts.yml
```
With that playbook Jenkins is ready to build test and install Bitamina D
