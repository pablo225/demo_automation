# ANSIBLE TO BUILD A KUBERNETES CLUSTER
We will use an opensource project for this task. [kubespray](https://kubespray.io/#/)

Kubespray has been extracted on ansible/kubernetes/kubespray


## Methods to build a k8s cluster using Ansible Kubespray project

**Running Vagrant and Ansible**

Vagrant can create the VMs to host the Vanilla Kubernetes Cluster provisioning them (including the k8s cluster) with Ansible. And all in one shot.
Following the steps below we will integrate with Vagrant the workflow of creating the VMs, bootstrapping them, and creating the Kubernetes Cluster.

Edit the Vagrantfile to suit your needs on cluster members, inventory to use, golden image to use, etc... Running it as it will bring up 3 cluster members which is sufficient for testing purposes.

Before running it, verify you have installed python3 and pip3. Without them the provisioning tasks will fail.
```ShellSession
% python -V && pip -V
....
```
Also install the necessary requirements with sudo or privileged user.
```ShellSession
% sudo pip install -r requirements.txt
....
```
Finally launch the build:
```ShellSession
% cd ansible/kubernetes/kubespray/
  vagrant up
....
```
More info about how to customize your vagrant here: [link](../../ansible/kubernetes/kubespray/docs/vagrant.md)

**Vagrant first, Ansible after**

With this case what we want to show is that, actually, Kubespray can also install the Kubernetes Cluster in existing VMs.
To show this approach working we have enabled vagrant/kubernetes/Vagrantfile that can bring the number of boxes desired [](../vagrant/README.md).
After, to build the Kubernetes Cluster on them we must call
```
% cd ansible/kubernetes/kubespray/
  ansible-playbook -i inventory/bitaminad/hosts.yaml  --become --become-user=root cluster.yml
```

### How to access to the Kubernetes Cluster created?

Upon completion of the kubernetes cluster build, within the inventory directory (**ansible/kubernetes/kubespray/inventory/bitaminad/**) you will find 2 folders named artifacts and credentials that contain the kubeadmin certificate key and the admin.conf to use with kubectl as administrator.

### How do to validate the cluster is up and running?
```ShellSession
pablorg@Pablos-MacBook-Pro artifacts % ./kubectl --kubeconfig admin.conf get nodes
NAME    STATUS   ROLES                  AGE   VERSION
k8s-1   Ready    control-plane,master   15h   v1.20.4
k8s-2   Ready    control-plane,master   15h   v1.20.4
k8s-3   Ready    <none>                 15h   v1.20.4
```
