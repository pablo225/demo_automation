## Docker Registry to store BITAMINA D app and other customized Docker images.
While running containerized apps we need a secure Docker Registry that usually is located in an enterprised solution like Artifactory or in the Cloud, such as GCR or Azure Container Registry.

Again, for the purpose of showcase we have decided to enable a selfcontained simple Docker Registry to be implemented as below:
- [x] Bring up a VM using vagrant. If you want just one VM for the registry set k8s_nodes =  1 in vagrant/kubernetes/Vagrantfile.
The installer will create a folder named credentials. Within it, you will find the first admin password.
```ShellSession
cd vagrant
vagrant up
```
 For this purpose we just need one VMs which IPs, by default will be: 192.168.56.12
- [x] Get docker.io installed on the VM
```ShellSession
ssh -i ssh/bitaminad vagrant@192.168.56.12
sudo apt install docker.io
```
- [x] Run a registry v2 container
```ShellSession
sudo docker run -d -p 5000:5000 --restart=always --name registry registry:2
```

This registry serves the purpose of testing.
For production grade, it should be replaced with an Azure/GC/AWS registry or other corporate ones.

A specific Vagrantfile can be created with provisioning tasks that perform the registry creation.
