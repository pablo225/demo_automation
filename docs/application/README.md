# Bitamina D Web Application
Bitamina D is a cutting edge application capable of posting notes on a board upon signup and authentications.
All notes get stored on an SQLAlchemy DB. So, amazingly they stay after signing out.

## MANUAL Setup & Installation

Make sure you have the latest stable version of Python installed.

```bash
git clone https://gitlab.com/pablo225/demo_automation.git
```
### Install the requirements

```bash
pip install -r requirements.txt
```

### Running The App

```bash
python main.py
```

### Verify the Bitamina D is up and running

Go to `http://127.0.0.1:5000`

## AUTOMATED Setup & Installation

Login on Jenkins http://<your_jenkins_ip_setup_before>:8080/

Find the pipeline demo_automation

![demo_automation](../../imgs/pipelines.png)

Launch 'build now' and check the build happens correctly
![demo_automation](../../imgs/runningpipeline.png)

Check the application is successfully deployed in the kubernetes cluster

http://<kubernetes_node_ips>:30020/
![demo_automation](../../imgs/application.png)
