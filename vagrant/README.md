# Vagrant to build up the infrastructure using our golden image
Upon creating the gold image we can use it to bring up the VMs of our solution. Machines are provisioned on top of VirtualBox, VMware, AWS, or any other provider.
For now we will showcase for VirtualBox.

## Vagrant
About what is Vagrant and how it can be used, see Vagrant Intro [link](https://www.vagrantup.com/intro)

## Requirements
The system where the build and upload will run must have:
- **Vagrant**

(MacOS)
```ShellSession
% brew install vagrant
....
% vagrant version
Installed Version: 2.2.19
Latest Version: 2.2.19
```
(Linux)
```ShellSession
% sudo yum install vagrant #RH family
....
% vagrant version
Installed Version: 2.2.19
Latest Version: 2.2.19
```
- **VirtualBox**

(MacOS)
```ShellSession
% brew install virtualbox
....
% vboxmanage --version
6.1.30r148432
```
(Linux)
```ShellSession
% sudo yum install virtualbox #RH family
....
% vboxmanage --version
6.1.30r148432
```
- **Bitamina D Golden Image**
```ShellSession
% cd demo_automation/vagrant
% vagrant box add bitaminad-gi-ubuntu-20.04 ../packer/artifacts/virtualbox/ubuntu-20.04-virtualbox.box
```
## How to create VMs to run a k8s cluster and Docker Registry 

### Clone of the git repo and cd the packer folder:

(MacOS or Linux)
```ShellSession
% git clone -b <desired-branch> https://gitlab.com/pablo225/demo_automation.git
% cd demo_automation/vagrant/kubernetes
```
### Run the build
(MacOS or Linux)
```ShellSession
% vagrant up
% vagrant ssh k8s1  # or
  ssh -i ssh/bitaminad vagrant@192.168.56.12
```
Few notes on the Vagrantfile:
1. It brings 4 VMs up. Set the number to suits you (4 for k8s cluster + 1 for Docker Registry, for example)
2. The etcd servers for the k8s cluster will run  in the same VMs. More flexibility on that respect is pending on implementation.
3. The purpose of this Vagrantfile is to create the infrastructure and make the systems visible to each other, while kubespray will do the setup.

### Next Steps:
#### Build the k8s cluster on the VMs created

[link to build k8s cluster](../kubernetes/README.md)

#### Build the Docker Registry on one of the VM's Created

[link to build a Docker Registry](../registry/README.md)
