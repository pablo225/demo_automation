#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

# TODO
echo "This Script should validate the services installed and setup are up and running"
echo "I will skip this step for now as we are going to use ansible to setup them up"
echo "After I will implement them"
