set -o errexit
set -o nounset
set -o pipefail

K8S_NODES="$1"
sudo apt-get install ufw

sudo timedatectl set-timezone America/Toronto
systemctl enable ufw
systemctl start  ufw

HOST_NAME=$(hostname --short)
HOST_IP=$(hostname -I | awk '{print $2}')
for (( i=1; i<=$K8S_NODES; i++ ))
do
   BASE_IP="192.168.56."
   OCTATE=$(($i + 10))
   if [ `cat /etc/hosts | grep "$BASE_IP$OCTATE  k8s$i  k8s$i.local" |wc -l` -eq 0 ]; then
      echo "$BASE_IP$OCTATE  k8s$i  k8s$i.local" >> /etc/hosts
   fi
done
